using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaceObject : MonoBehaviour
{
    // Alustetaan muuttujat
    public GameObject objectToPlace;
    private ARRaycastManager _arRaycast;
    private Vector2 touchPosition;
    private float spawndelay = 0;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    void Awake()
    {
        _arRaycast = GetComponent<ARRaycastManager>(); // Tallennetaan ar-raycast manager
    }


    bool detectTouch(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position; // asetetaan kosketuskohta muuttujaan touchPosition
            return true; // Jos kosketaan näyttöön palautetaan true
        }
        else
        {
            touchPosition =  new Vector2(0,0); // Asetetaan touchPosition origoon
            return false; // Jos ei kosketa näyttöön, palautetaan false
        }
    }
    
    void Update()
    {
        spawndelay = spawndelay + Time.deltaTime;
        if (!detectTouch(out Vector2 touchPosition)) return; // Jos näyttöön ei kosketa, ei tehdä mitään

        if(_arRaycast.Raycast(touchPosition,hits,TrackableType.Planes) && spawndelay > 1f) // Jos näyttöön kosketaan, ja spawndelay on kiivennyt yli sekunnin ->
        {
            spawndelay = 0; // Asetetaan spawndelay nollaan
            Instantiate(objectToPlace, hits[0].pose.position, hits[0].pose.rotation); // Näyttöön koskettaessa asettaa kuution koskettuun paikkaan
            
        }
        
    }
}