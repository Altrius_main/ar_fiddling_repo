using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubemover : MonoBehaviour
{
    // Alustetaan laskuri ja kerroinmuuttuja kuution liikuttelua varten
    private float counter = 0;
    private float multiplier = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        counter = counter + Time.deltaTime * multiplier; // Lisätään kulunut aika laskuriin kertoimen ollessa 1, ja vähennetään kertoimen ollessa -1
        if (counter > 1f) multiplier = -1f; // Käännetään kerroin ylärajassa
        if (counter < 0) multiplier = 1f; // Käännetään kerroin alarajassa

        transform.Translate(Vector3.up * Time.deltaTime * multiplier * 0.1f, Space.World); // Siirretään kuutiota kuluneen ajan suhteen siten, että multiplier määrää kuution liikkeen suunnan
    }
}
